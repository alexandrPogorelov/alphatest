package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {

            //Здесь мы определяем ОС и в зависимости от нее выбираем разделитель пути к файлу.
            //Кажется, в File было что-то такое но у меня оно не работало, поэтому использую написанное руками
            //Да, на Маке скорее всего код работать не будет, если только там не такой же разделитель, как и в Windows
            //Конечно, при необходимости это решаемо.
            String os = System.getProperty("os.name");
            String pathSeparator = (os.equals("Linux")) ? "/":"\\";

            String path = (new File(".").getCanonicalPath())+pathSeparator+"in.txt";
            //System.out.println(path);
            Scanner scan = new Scanner(new FileInputStream(path));
            ArrayList<Integer> sortingArray = new ArrayList();

            /*Настраиваем разделитель сканера. В данном случае это регулярка от 1 до n нечисловых символов.
             Да, можно было написать просто написать запятую, но так универсальнее.
            */

            scan.useDelimiter("\\D+");

            //В sortingArray считываем сканнером все найденные в файле integer
            while(scan.hasNext()) {
                sortingArray.add(scan.nextInt());
            }

            /*Для сортировки используется уже готовый метод из Collections. Обычно я стараюсь проверить нет ли в языке уже готового инструмента под проблему,
            применим ли он к моей ситуации, если все ок использую. Да, в данном случае можно было бы отсортировать массив каким-нибудь пузырьком, но не вижу в этом смысла,
            так как есть уже раализованная быстрая сортировка в методе sort.
             */
            Collections.sort(sortingArray);
            printArrayList(sortingArray);
            Collections.reverse(sortingArray);
            printArrayList(sortingArray);
        }
        catch(java.io.FileNotFoundException e){
            e.printStackTrace();
        }
        catch (java.io.IOException e) {
            e.printStackTrace();
        }

        System.out.println(factorial(20));
    }
    public static void printArrayList(ArrayList arrayPrint){
        //На данный момент нет привычки решать такие проблемы лямбдами.
        //Осваивю, тк они полезны и помогают избавляться от лишнего кода.
        arrayPrint.forEach(i -> System.out.print(i+" "));
        System.out.println();
    }
    public static long factorial(int number){
        //Готового факториала не нашел, написал метод считающий факториал влоб. Да, можно было бы придумать что-то через
        // рекурсию, или вообще поискать какие-то более эффективвные подходы, но для данного факториала достаточно такого решения
        // В данном случае использованного long хватает, для ситуации
        // если бы long не хватило, использовал бы объект BigInteger
        long rezult=1;
        for (int i=1; i<number; i++)
            rezult=rezult*i;
        return rezult;
    }
}